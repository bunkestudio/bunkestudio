#!/usr/bin/env python
# -*- coding: utf-8 -*- #
from __future__ import unicode_literals

AUTHOR = 'Jara & Fernández'
SITENAME = 'Bunk'
SITEURL = ''

PATH = 'content'
OUTPUT_PATH = 'public/'

TIMEZONE = 'America/Buenos_Aires'

DEFAULT_LANG = 'es'

WITH_FUTURE_DATES=True

STATIC_PATHS = ['images', 'pdfs', 'audios' ]

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
LINKS = (
  ('Pelican', 'http://getpelican.com/'),
  ('Se pueden agregar', '#'),
)

# Social widget
#SOCIAL = (('You can add links in your config file', '#'),
#          ('Another social link', '#'),)

DEFAULT_PAGINATION = 10

# Uncomment following line if you want document-relative URLs when developing
# RELATIVE_URLS = True

THEME = 'pelican-alchemy/alchemy'

SITESUBTITLE = 'A magical \u2728 Pelican theme'
SITEIMAGE = '/images/profile.png'
SITEIMAGE = '/images/profile.svg width=200 height=200'
DESCRIPTION = 'A functional, clean, responsive theme for Pelican. Heavily ' \
              'inspired by crowsfoot and clean-blog, powered by Bootstrap.'
ICONS = (
    ('instagram', 'https://www.instagram.com/bunk.music/'),
    ('gitlab', 'https://gitlab.com/bunk.studio'),
)

# FOOTER_LINKS: A list of tuples (Title, URL) for footer links. Replaces default set of links (Authors, Archives, Categories, Tags).

# BOOTSTRAP_CSS: URL of Bootstrap CSS file. Use this to enable Boostwatch themes.
# FONTAWESOME_CSS: URL of Font Awesome CSS file. Use this if you wish to use CDN provided version instead of the bundled one.

PYGMENTS_STYLE = 'paraiso-dark'
# Built-in Pygments style for syntax highlighting.
# algol
# algol_nu
# autumn
# borland
# bw
# colorful
# default
# emacs
# friendly
# fruity
# igor
# lovelace
# manni
# monokai
# murphy
# native
# paraiso-dark
# paraiso-light
# pastie
# perldoc
# rrt
# tango
# trac
# vim
# vs
# xcode

#HIDE_AUTHORS = True
#RFG_FAVICONS = True
#DISQUS_SITENAME = '...'
#GAUGES = '...'
#GOOGLE_ANALYTICS = '...'
#PIWIK_URL = '...'
#PIWIK_SITE_ID = '...'


# HIDE_AUTHORS: Hide the author(s) of an article - useful for single author sites.
# RFG_FAVICONS: Use a Favicon Generator package.
# THEME_CSS_OVERRIDES: Sequence of stylesheet URLs to override CSS provided by theme. Both relative and absolute URLs are supported.
# THEME_JS_OVERRIDES: Sequence of JavaScript URLs to enable with this theme. Alchemy uses no JS by default. Both relative and absolute URLs are supported.
